# sample flask-opa

Sample Code for flask-opa

## 介紹

Easy way to create flask project with flask-opa sample code.

## 啟動opa服務並讀入判斷規則檔
1. opa run -s  app.rego -w

## 啟動flask
2. export FLASK_ENV=development
3. export FLASK_APP=app.py
4. flask run

## opa讀入資料檔
5. curl -X PUT http://localhost:8181/v1/data --data-binary @data.json
## 執行測試
6. curl -X GET http://localhost:5000/orders -H 'Content-Type: application/json' -H 'Authorization: U02322f3c25e40f3b7983b9960cc18f33'
