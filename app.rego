package app.rbac
import future.keywords.in

default allow = false

user_is_admin {
	"admin" in data.user_roles[input.user]
}
allow {
	user_is_admin
}

allow {
	some grant
	user_is_granted[grant]
	input.method in grant.method
	grant.path in input.path
}

user_is_granted[grant] {
	some role in data.user_roles[input.user]
	some grant in data.role_grants[role]
}
